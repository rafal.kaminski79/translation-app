package org.transaltion.rs.controllers;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeTranslationControllerIT extends TranslationControllerTest {

    // Execute the same tests but in native mode.
}