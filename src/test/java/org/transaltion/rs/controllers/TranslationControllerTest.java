package org.transaltion.rs.controllers;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.transaltion.domain.daos.TranslationDAO;
import org.transaltion.domain.models.Translation;
import org.transaltion.rs.models.RankingDTO;
import org.transaltion.rs.models.TranslationDTO;

import java.util.Collections;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@QuarkusTest
public class TranslationControllerTest {

    @InjectMock
    TranslationDAO translationDAO;

    @Test
    public void testGetTranslatedSentence() {
        //given
        String polishSentence = "Ala ma";
        Translation translationAla = Translation.builder().englishTranslation(Collections.singletonList("Alice")).countCalls(0L).id(0L).polish("Ala").build();
        Translation translationMa = Translation.builder().englishTranslation(Collections.singletonList("has")).countCalls(0L).id(0L).polish("ma").build();
        Mockito.when(translationDAO.findEnglishTranslation("Ala")).thenReturn(translationAla);
        Mockito.when(translationDAO.findEnglishTranslation("ma")).thenReturn(translationMa);
        String expectedSentence = "Alice has";
        String expectedQuotedSentence = "\"Alice\" \"has\"";

        //when
        Response response = given()
                .when()
                .body(polishSentence)
                .post("/translation");

        //then
        response.then().statusCode(200);
        verify(translationDAO, times(2)).update(any());
        TranslationDTO translationDTO = response.as(TranslationDTO.class);
        assertEquals(expectedSentence, translationDTO.getTranslatedSentence());
        assertEquals(expectedQuotedSentence, translationDTO.getTranslateQuotedWords());
    }

    @Test
    public void testGetTranslationsAscending() {
        //given
        Translation translationCount1 = Translation.builder().englishTranslation(Collections.singletonList("Alice")).countCalls(0L).id(1L).polish("Ala").build();
        Translation translationCount2 = Translation.builder().englishTranslation(Collections.singletonList("has")).countCalls(0L).id(2L).polish("ma").build();
        Translation translationCount11 = Translation.builder().englishTranslation(Collections.singletonList("fight")).countCalls(0L).id(1L).polish("walcz").build();
        Mockito.when(translationDAO.getTranslationCallsDescending()).thenReturn(List.of(translationCount11, translationCount2, translationCount1));

        //when
        Response response = given()
                .when()
                .get("/translation/ranking");

        //then
        response.then().statusCode(200);
        RankingDTO[] rankingList = response.as(RankingDTO[].class);
        assertEquals(3L, rankingList.length);
    }
}