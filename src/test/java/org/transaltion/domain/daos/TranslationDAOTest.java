package org.transaltion.domain.daos;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.transaltion.domain.models.Translation;

import javax.inject.Inject;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
public class TranslationDAOTest {

    @Inject
    TranslationDAO translationDAO;

    @Test
    @Order(1)
    public void testGetEnglishTranslation() {
        //given
        String expectedTranslation = "a cat";
        String polishWord = "kota";

        //when
        Translation translation = translationDAO.findEnglishTranslation(polishWord);

        //then
        assertNotNull(translation);
        assertNotNull(translation.getEnglishTranslation());
        assertEquals(expectedTranslation, translation.getEnglishTranslation().get(0));
    }

    @Test
    @Order(2)
    public void testUpdateTranslation() {
        //given
        Long expectedCountCalls = 9999L;
        String polishWord = "kota";
        Translation translation = translationDAO.findEnglishTranslation(polishWord);
        translation.setCountCalls(expectedCountCalls);

        //when
        translationDAO.update(translation);

        //then
        Translation translationUpdated = translationDAO.findEnglishTranslation(polishWord);
        assertEquals(expectedCountCalls, translationUpdated.getCountCalls());
    }

    @Test
    @Order(3)
    public void testGetTranslationsDescending() {
        //given
        //when
        List<Translation> translationList = translationDAO.getTranslationCallsDescending();

        //then
        assertEquals(11L, translationList.size());
        assertEquals(9999L, translationList.get(0).getCountCalls());
        assertEquals(0L, translationList.get(1).getCountCalls());
        assertEquals(0L, translationList.get(2).getCountCalls());
    }
}
