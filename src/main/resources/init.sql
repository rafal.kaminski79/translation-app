INSERT INTO T_TRANSLATION (id, polish, count_calls)
VALUES (1, 'Ala', 0), (2, 'ma', 0), (3, 'kota', 0), (4, 'jesteś', 0),
(5, 'sterem', 0),(6, 'białym', 0),(7, 'żołnierzem', 0),(8, 'nosisz', 0),
(9, 'spodnie', 0),(10, 'więc', 0),(11, 'walcz', 0);

INSERT INTO T_ENGLISH_TRANSLATION (translation_id, english_translation)
VALUES (1, 'Alice'),(2, 'has'),(3, 'a cat'),(4, 'you are'),(5, 'the helm'),(6, 'white'),(7, 'soldier'),(8, 'wear'),(9, 'trousers'),(10, 'so'),(11, 'fight');