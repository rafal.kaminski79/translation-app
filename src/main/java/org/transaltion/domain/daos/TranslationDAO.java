package org.transaltion.domain.daos;

import org.transaltion.domain.models.Translation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class TranslationDAO {

    private final String POLISH = "polish";
    private final String COUNT_CALLS = "countCalls";

    @Inject
    EntityManager em;

    public Translation findEnglishTranslation(String polish) {
        CriteriaQuery<Translation> cq = em.getCriteriaBuilder().createQuery(Translation.class);
        cq.where(cq.from(Translation.class).get(POLISH).in(polish));
        TypedQuery<Translation> query = em.createQuery(cq);
        return query.getResultStream().findFirst().orElse(null);
    }

    public List<Translation> getTranslationCallsDescending() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Translation> cq = cb.createQuery(Translation.class);
        Root<Translation> root = cq.from(Translation.class);
        cq.orderBy(cb.desc(root.get(COUNT_CALLS)));
        TypedQuery<Translation> query = em.createQuery(cq);
        return query.getResultList();
    }

    @Transactional
    public void update(Translation translation) {
        em.merge(translation);
        em.flush();
    }
}
