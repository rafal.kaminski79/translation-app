package org.transaltion.domain.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Table(name = "T_TRANSLATION")
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Translation {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "POLISH")
    private String polish;

    @Column(name = "COUNT_CALLS")
    private Long countCalls;

    @ElementCollection
    @CollectionTable(name = "T_ENGLISH_TRANSLATION", joinColumns = @JoinColumn(name = "TRANSLATION_ID"))
    @Column(name="ENGLISH_TRANSLATION")
    private List<String> englishTranslation;
}
