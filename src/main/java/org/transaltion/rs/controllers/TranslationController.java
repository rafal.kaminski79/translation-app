package org.transaltion.rs.controllers;

import org.transaltion.domain.daos.TranslationDAO;
import org.transaltion.domain.models.Translation;
import org.transaltion.rs.mappers.TranslationMapper;
import org.transaltion.rs.models.TranslationDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Path("/translation")
@ApplicationScoped
public class TranslationController {

    @Inject
    TranslationDAO translationDAO;

    @Inject
    TranslationMapper translationMapper;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response getTranslation(String sentence) {

        String[] words = sentence.split(" ");
        List<Translation> translations = findTranslations(words);
        translations.forEach(this::increaseCountCall);
        List<String> translatedWords = getEnglishTranslations(translations);

        return Response.status(200).entity(constructTranslatedDTO(translatedWords)).build();
    }

    @GET
    @Path("/ranking")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRanking() {
        List<Translation> translations = translationDAO.getTranslationCallsDescending();

        return Response.status(200).entity(translationMapper.map(translations)).build();
    }

    private List<String> getEnglishTranslations(List<Translation> translations) {
        return translations
                .stream()
                .map(t -> t.getEnglishTranslation().get(0))
                .collect(Collectors.toList());
    }

    private List<Translation> findTranslations(String[] words) {
        return Arrays
                .stream(words)
                .map(w -> translationDAO.findEnglishTranslation(w))
                .collect(Collectors.toList());
    }

    private void increaseCountCall(Translation t) {
        t.setCountCalls(t.getCountCalls() + 1);
        translationDAO.update(t);
    }

    private List<String> getQuotedWords(List<String> translatedWords) {
        return translatedWords.stream().map(w -> '"' + w + '"').collect(Collectors.toList());
    }

    private TranslationDTO constructTranslatedDTO(List<String> translatedWords) {
        TranslationDTO translationDTO = new TranslationDTO();
        translationDTO.setTranslatedSentence(String.join(" ", translatedWords));
        translationDTO.setTranslateQuotedWords(String.join(" ", getQuotedWords(translatedWords)));
        return translationDTO;
    }
}
