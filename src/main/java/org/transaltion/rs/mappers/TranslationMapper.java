package org.transaltion.rs.mappers;

import org.transaltion.domain.models.Translation;
import org.transaltion.rs.models.RankingDTO;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class TranslationMapper {

    public List<RankingDTO> map(List<Translation> translations) {
        List<RankingDTO> rankingList = new ArrayList<>();
        translations.forEach(t -> rankingList.add(new RankingDTO(t.getPolish(), t.getCountCalls())));
        return rankingList;
    }
}
