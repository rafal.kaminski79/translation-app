package org.transaltion.rs.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TranslationDTO {

    private String translatedSentence;

    private String translateQuotedWords;

}
