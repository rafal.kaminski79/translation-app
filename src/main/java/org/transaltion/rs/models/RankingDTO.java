package org.transaltion.rs.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RankingDTO {

    private String word;

    private Long countCalls;
}
